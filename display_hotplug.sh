#!/bin/sh

# Query with xrandr to get the list of available modes
# Since this script is called from the udev rule, 10-openxtfb.rules, it will be run when a new mode has been added
# Run xrandr a second time, X seems to provide more information on new modes during the second query
# Then switch to the newly detected, preferred mode using the --auto flag
xrandr -display :0 --query
xrandr -display :0 --query
xrandr -display :0 --output virtual --auto
