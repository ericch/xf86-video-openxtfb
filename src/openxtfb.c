/**
 * OpenXT Framebuffer ("openxtfb") Driver for X11
 * Authors: Kyle J. Temkin <temkink@ainfosec.com>
 *
 * Based on the fbdev driver, by:
 * Authors:
 *       Alan Hourihane, <alanh@fairlite.demon.co.uk>
 *       Michel Dänzer, <michel@tungstengraphics.com>
 */

#include "openxtfb.h"

/**
 * Define the OpenXT framebuffer DDX (Device Dependent X) driver.
 * This provides the interface Xorg uses to interact with the driver.
 */
_X_EXPORT DriverRec openxtfbDriver =
{
    .driverVersion    = OPENXTFB_VERSION,
    .driverName       = "OpenXT-FB",
    .Identify         = openxtfbIdentify,
    .Probe            = openxtfbProbe,
    .AvailableOptions = openxtfbGetPossibleOptions,
    .module           = NULL,
    .refCount         = 0,
    openxtfbDriverFunction
};

/**
 * Dymnamically loaded driver support.
 */
#ifdef XFree86LOADER

//Use our setup function as our entry-point.
MODULESETUPPROTO(openxtfbSetup);

/**
 * Store information about the driver, for use when loaded dynamically
 * as a module.
 */
static XF86ModuleVersionInfo openxtfbVersionInformation =
{
    .modname      = "openxtfb",
    .vendor       = "Assured Information Security, Inc.",
    ._modinfo1_   = MODINFOSTRING1,
    ._modinfo2_   = MODINFOSTRING2,
    .xf86version  = XORG_VERSION_CURRENT,
    .majorversion = PACKAGE_VERSION_MAJOR,
    .minorversion = PACKAGE_VERSION_MINOR,
    .patchlevel   = PACKAGE_VERSION_PATCHLEVEL,
    .abiclass     = ABI_CLASS_VIDEODRV,
    .abiversion   = ABI_VIDEODRV_VERSION,
    .moduleclass  = NULL,
    .checksum     = {0, 0, 0, 0}
};

//... and export the module information for external access.
_X_EXPORT XF86ModuleData openxtfbModuleData = 
    { &openxtfbVersionInformation, openxtfbSetup, NULL };


/**
 * Internal method which intializes the plugin, when loaded dynamically.
 *
 * @param module A reference to the module to be loaded.
 * @param opts Any options provided to the module at loadtime.
 * @param errmaj Out argument. If provided, this variable will be populated with an error code
 *    on failure, allowing NULL returns to be diagnosed.
 * @param errmin Out argument. Same as errmaj, but for minor error numbers.
 *
 * @return NULL on failure, or an (invalid) pointer on success. This is really ugly, but it's
 *    the way the X server does things, for historic reasons. Ostensibly, we could also return
 *    an opaque pointer, if we had a need to do so.
 */
pointer openxtfbSetup(pointer module, pointer opts, int *errmaj, int *errmin)
{
    //Singleton guard. Ensures that this module is only ever loaded once.
    static Bool setupDone = FALSE;
  
    //If we've already loaded this module, fail out!
    if(setupDone)
    {
        //If we've been provided with an error code out argument, populate it
        //with the relevant error code.
        if(errmaj) 
          *errmaj = LDR_ONCEONLY;

        return NULL;
    }

    //Register the openxt framebuffer driver with the X server...
    xf86AddDriver(&openxtfbDriver, module, HaveDriverFuncs);

    //... mark this module as loaded...
    setupDone = TRUE;

    //... and indicate success. This is a really ugly way to indicate succss,
    //but it's the conventional way to do it. Historically, we could ostensibly populate 
    //this with an opaque pointer, but we have no need. NULL, unfortunately, is an error
    //condition.
    return (pointer)1;
}

#endif 


/**
 * Creates a new openxtfb private information record, which encapsulates
 * the state of the drvier.
 *
 * @param pScrn The screen for which the driver record is to be created.
 * @return The newly-created record, or NULL on failure.
 */
static openxtfbRec * createOpenxtfbObject(ScrnInfoPtr pScrn)
{
    //If we don't already have a private object, allocate one!
    if(!pScrn->driverPrivate)
        pScrn->driverPrivate = xnfcalloc(sizeof(openxtfbRec), 1);

    //... and return the relevant record.
    return (openxtfbRec *)pScrn->driverPrivate;
}


/** 
 * Destroys an openxtfb private information record, freeing the memory
 * associated with the driver.
 *
 * @param pScrn The screen for which the driver record is to be removed.
 */
static void destroyOpenxtfbObject(ScrnInfoPtr pScrn)
{
    if(pScrn->driverPrivate == NULL)
        return;

    free(pScrn->driverPrivate);
    pScrn->driverPrivate = NULL;
}


/**
 * @return An array of the supported options that can be specified in xorg.conf.
 */
static const OptionInfoRec * openxtfbGetPossibleOptions(int chipid, int busid)
{
    return openxtfbOptions;
}


/**
 * Prints out a set of identifying information, so a human reading the logs
 * can identify information about the driver in use. We follow the X11
 * convention of printing out the supported "chipsets".
 *
 * @param flags Unused.
 */
static void openxtfbIdentify(int flags)
{
    SymTabRec chipsets[] =
    {
        { 0, "openxtfb" },
        { -1, NULL }
    };

    xf86PrintChipsets(OPENXTFB_NAME, "driver for framebuffer", chipsets);
}

/**
 * Attempts to create a connection to an OpenXT framebuffer.
 *
 * @param drv The information structure for the currently loaded driver.
 * @param flags Indicates whether we're really trying to load the driver (0)
 *    or just trying to detect the device's presence (PROBE_DETECT).
 *
 *
 * @return True on success, or false on failure.
 */
static Bool openxtfbProbe(DriverPtr drv, int flags)
{
    int i;
    char *dev;
    GDevPtr *devSections;
    int numDevSections;
    Bool foundScreen = FALSE;

    //For now, we don't handle PROBE_DETECT.
    //Neither do most of the other DDX drivers we've seen, so this is probably fine.
    if(flags & PROBE_DETECT)
        return FALSE;

    //TODO: Bail out here if we don't have an OpenXT framebuffer device.
    //Right now, without bailing, we try use the relevant framebuffer even
    //if it's not an OpenXT one.

    //If we don't have any driver sections that try to use an OpenXT framebuffer
    //device, fail out. Note that this doesn't interfere with autodetection--
    //it seems that autodetection generates a valid device section.
    if((numDevSections = xf86MatchDevice(OPENXTFB_DRIVER_NAME, &devSections)) <= 0)
        return FALSE;

    //Load the submodule used to communicate directly with fbdev devices.
    //If it fails to load, fail out!
    if(!xf86LoadDrvSubModule(drv, "fbdevhw"))
        return FALSE;


    //For each of the display drivers in the Xorg configuration...
    for(i = 0; i < numDevSections; i++)
    {
        //... determine the actual framebuffer device that represents our
        //OpenXT framebuffer.
        dev = (char *)xf86FindOptionValue(devSections[i]->options, "fbdev");

        //Attempt to make a connection to the raw framebuffer device.
        //If we can, use it!
        if(fbdevHWProbe(NULL, dev, NULL))
        {
            int entity;
            ScrnInfoPtr pScrn;
  
            //Mark this as the piece of hardware that will provide the framebuffer
            //for the given driver entry.
            entity = xf86ClaimFbSlot(drv, 0, devSections[i], TRUE);

            //And attempt to set up the X screen that corresponds to the framebuffer.
            pScrn = xf86ConfigFbEntity(NULL, 0, entity, NULL, NULL, NULL, NULL);

            //If we were able to set up the X screen, populate its methods.
            //This enables X to draw on the screen!
            if(pScrn)
            {
                foundScreen = TRUE;

                //Use our OpenXT-fb specific functions for most of the
                //screen functions...
                pScrn->driverVersion = OPENXTFB_VERSION;
                pScrn->driverName    = OPENXTFB_DRIVER_NAME;
                pScrn->name          = OPENXTFB_NAME;
                pScrn->Probe         = openxtfbProbe;
                pScrn->PreInit       = openxtfbPreInit;
                pScrn->ScreenInit    = openxtfbScreenInit;
                pScrn->SwitchMode    = openxtfbSwitchMode;

                //... and use the generic fbdev functions for the remainder.
                pScrn->AdjustFrame   = fbdevHWAdjustFrameWeak();
                pScrn->EnterVT       = fbdevHWEnterVTWeak();
                pScrn->LeaveVT       = fbdevHWLeaveVTWeak();
                pScrn->ValidMode     = fbdevHWValidModeWeak();

                //Print out information regarding which framebuffer device file
                //is being used, for debug.
                xf86DrvMsg(pScrn->scrnIndex, X_INFO, "using %s\n", dev ? dev : "default device");
            }
        }
    }

    //Finally, clean up.
    free(devSections);
    return foundScreen;
}


/**
 * Sets up the driver to use mode and geometry information
 * from the OpenXT framebuffer device.
 *
 * @param pScrn The relevant Xorg screen.
 * @param oxtPtr The OpenXT framebuffer driver record.
 */
static void openxtfbSetUpGeometry(ScrnInfoPtr pScrn, openxtfbPtr oxtPtr)
{
    int rc;
    static struct openxtfb_size_hint size_hint;

    //Create a copy of the framebuffer's built-in mode, and use this
    //to set up an initial mode. This will quickly be overridden by
    //the RandR configuration, but we need an initial mode to start up.
    pScrn->modes = xf86DuplicateMode(fbdevHWGetBuildinMode(pScrn));
    pScrn->modes->next = pScrn->modes;
    pScrn->modes->prev = pScrn->modes;
    pScrn->currentMode  = pScrn->modes;

    //Ask the OpenXT framebuffer for information regarding its geometry...
    rc = ioctl(oxtPtr->fd, OPENXTFB_IOCTL_GET_SIZE_HINT, &size_hint);

    //If we couldn't read the size information from openxtfb,
    //fall back to the current display.
    if(rc) {
      xf86Msg(X_WARNING, "openxtfb: Couldn't read "
          " size hints. Falling back to current.\n");
      pScrn->virtualX     = pScrn->currentMode->HDisplay;
      pScrn->virtualY     = pScrn->currentMode->VDisplay;
      pScrn->displayWidth = fbdevHWGetLineLength(pScrn) / openxtfb_bytes_per_pixel;
    }

    //Store our fixed modesetting information...
    oxtPtr->virtualX    = size_hint.max_width;
    oxtPtr->virtualY    = size_hint.max_height;
    oxtPtr->stride      = size_hint.stride / openxtfb_bytes_per_pixel;

    //... and apply it to the X screen.
    pScrn->virtualX     = oxtPtr->virtualX;
    pScrn->virtualY     = oxtPtr->virtualY;
    pScrn->displayWidth = oxtPtr->virtualX;
}


/**
 * Performs the early initialization for our openxt framebuffer driver.
 *
 * @param pScrn The Xorg screen that our driver will be managing.
 * @param flags Indicates whether we're really trying to load the driver (0)
 *    or just trying to detect the device's presence (PROBE_DETECT).
 */
static Bool openxtfbPreInit(ScrnInfoPtr pScrn, int flags)
{
    openxtfbPtr oxtPtr;
    int default_depth, fbbpp;
    int type;

    char *dev;

    //Don't perform any initialization if we're just trying to detect
    //the screen's presence.
    if(flags & PROBE_DETECT) 
      return FALSE;

    //The openxtfb driver supports only one screen per device-- as each
    //device is literally a represnetation of a framebuffer. If we've been
    //asked for any other number of screens, fail out.
    if(pScrn->numEntities != 1)
        return FALSE;

    //Use the monitor object provided by the configuration directly.
    pScrn->monitor = pScrn->confScreen->monitor;

    //Allocate the driver's private information structure...
    oxtPtr = createOpenxtfbObject(pScrn);

    //Set a sane initial cursor color, so we have a visible cursor even if
    //no proper colors are set.
    oxtPtr->cursorFG = 0xFF000000;    

    //... and bind it to the screen.
    oxtPtr->pEnt = xf86GetEntityInfo(pScrn->entityList[0]);

    //Retreive the name of the framebuffer device to be used from the config,
    //if one was provided.
    dev = (char *)xf86FindOptionValue(oxtPtr->pEnt->device->options, "fbdev");

    //Try to create a connection to the underlying framebuffer. If we couldn't, fail out.
    if(!fbdevHWInit(pScrn, NULL, dev))
        return FALSE;

    //And retreive the file-descriptor we'll use for a direct framebuffer connection.
    oxtPtr->fd = fbdevHWGetFD(pScrn);

    //Set up the framebuffer to 
    default_depth = fbdevHWGetDepth(pScrn, &fbbpp);
    if(!xf86SetDepthBpp(pScrn, default_depth, default_depth, fbbpp, Support32bppFb))
        return FALSE;

    //Set the color format on the screen to ARGB:8888, which matches the backing
    //store of the underlying OpenXT framebuffer.
    rgb weights = { 8, 8, 8 };
    rgb masks   = { 0x00FF0000, 0x0000FF00, 0x000000FF };
    if(!xf86SetWeight(pScrn, weights, masks))
        return FALSE;

    //Use TrueColor by default, as there's no translation involved.
    if(!xf86SetDefaultVisual(pScrn, TrueColor))
        return FALSE;

    //Set up our basic internal record of the framebuffer's shape.
    openxtfbSetUpGeometry(pScrn, oxtPtr);

    //Set up a virtual output CRTC, which will enable us to use RandR.
    if(!openxtfbPrepareForRandR(pScrn, oxtPtr))
        return FALSE;

    //Use the default gamma for the given screen.
    Gamma zeros = {0.0, 0.0, 0.0};
    if(!xf86SetGamma(pScrn, zeros))
        return FALSE;

    //Set up the "simulated" properties of the screen...
    pScrn->progClock = TRUE;
    pScrn->chipset   = "openxtfb";

    //...and finally, map in our framebuffer.
    pScrn->videoRam  = fbdevHWGetVidmem(pScrn);

    //Read any remaining relevant options from the Xorg configuration...
    xf86CollectOptions(pScrn, NULL);
    if(!(oxtPtr->Options = malloc(sizeof(openxtfbOptions))))
        return FALSE;

    //... and let X handle them.
    memcpy(oxtPtr->Options, openxtfbOptions, sizeof(openxtfbOptions));
    xf86ProcessOptions(pScrn->scrnIndex, oxtPtr->pEnt->device->options, oxtPtr->Options);

    //Use the default display DPI...
    xf86SetDpi(pScrn, 0, 0);

    //Finally, attempt to load our xf86 framebuffer submodule.
    if(xf86LoadSubModule(pScrn, "fb") == NULL)
    {
        destroyOpenxtfbObject(pScrn);
        return FALSE;
    }

    //Indicate success!
    return TRUE;
}


/**
 * Convenience function. Returns the framebuffer fd associated with
 * the given screen.
 *
 * @param pScreen The screen whose framebuffer fd is to be located.
 * @return The file descriptor for the relevant screen.
 */
static int fdForScreen(ScreenPtr pScreen)
{
    //Get a referfence to the driver's internal data structure.
    ScrnInfoPtr pScrn = xf86Screens[pScreen->myNum];
    openxtfbPtr oxtPtr = (openxtfbPtr)pScrn->driverPrivate;

    //... and gets its file descriptor.
    return oxtPtr->fd;
}


/**
 * Convenience function. Returns the framebuffer fd associated with
 * the given ScreenInfo.
 *
 * @param pScreen The screen whose framebuffer fd is to be located.
 * @return The file descriptor for the relevant screen.
 */
static int fdForScrn(ScrnInfoPtr pScrn)
{
    //Get a referfence to the driver's internal data structure.
    openxtfbPtr oxtPtr = (openxtfbPtr)pScrn->driverPrivate;

    //... and gets its file descriptor.
    return oxtPtr->fd;
}



/**
 * Determines if use of a PV cursor is appropriate for the underlying
 * OpenXT framebuffer connection.
 *
 * @param pScreen The screen whose capabilities are to be queried.
 * @param pCurs The relevant hardware cursor object.
 *
 * @return TRUE iff we can use hardware cursors with this display.
 */
static Bool openxtfbUsePVCursor(ScreenPtr pScreen, CursorPtr pCurs)
{
    struct openxtfb_caps caps;

    //Get a referfence to the driver's internal data structure.
    ScrnInfoPtr pScrn = xf86Screens[pScreen->myNum];
    openxtfbPtr oxtPtr = (openxtfbPtr)pScrn->driverPrivate;

    //Get our file descriptor, which provides our linkage to the driver.
    int fb_fd = fdForScreen(pScreen);

    //Try to get the framebuffer's capabilities.
    if (ioctl(fb_fd, OPENXTFB_IOCTL_GET_CAPS, &caps) != 0) {
        xf86DrvMsg(pScreen->myNum, X_ERROR, "Could not properly interface with "
            "OpenXT framebuffer!\n");
        return FALSE;
    }

    //If we suppor the given cursor, stash its information for later.
    //Unfortunately, this seems to be the only place we have access to this
    //information; and this seems to be the place other cursor setups stash
    //their results.
    if(caps.supports_cursor) {
        oxtPtr->cursorHotspotX = pCurs->bits->xhot;
        oxtPtr->cursorHotspotY = pCurs->bits->yhot;
        return TRUE;
    } else {
        return FALSE;
    }
}


static void __openxtfbLoadCursorImageARGB(ScrnInfoPtr pScrn, void * new_image, 
    int width, int height, int hot_x, int hot_y)
{

    struct openxtfb_cursor_image image;
    struct openxtfb_cursor_properties properties;

    //Get our file descriptor, which provides our linkage to the driver.
    int fb_fd = fdForScrn(pScrn);

    //Load the rendered cursor image into OpenXT-fb.
    image.image  = new_image;
    image.width  = width;
    image.height = height;
    ioctl(fb_fd, OPENXTFB_IOCTL_LOAD_CURSOR_IMAGE, &image);

    //Finally, send the new cursor's hotspot.
    properties.hotspot_x = hot_x;
    properties.hotspot_y = hot_y;
    ioctl(fb_fd, OPENXTFB_IOCTL_SET_CURSOR_PROPERTIES, &properties);

}



#ifdef ARGB_CURSOR
/**
 * Determines if use of an ARGB cursor is appropriate for the underlying
 * OpenXT framebuffer connection.
 *
 * @param pScreen The screen whose capabilities are to be queried.
 * @param pCurs The relevant hardware cursor object.
 *
 * @return TRUE iff we can use ARGB cursors with this display.
 */
static Bool openxtfbUsePVCursorARGB(ScreenPtr pScreen, CursorPtr pCurs)
{
    //First, verify that we have general PV cursor support.
    if(!openxtfbUsePVCursor(pScreen, pCurs))
      return FALSE;

    //... and finally, make sure that the ARGB image will fit into our buffer.
    return pCurs->bits->height <= openxtfb_cursor_width_pixels &&
           pCurs->bits->width <= openxtfb_cursor_height_pixels;
}

/**
 * Loads an ARGB cursor into the openxtfb driver. This is the preferred case,
 * as it matches the native format used by the OpenXT framebuffer.
 *
 * @param pScrn The screen for which the cursor should be loaded.
 * @param pCurs The cursor to be loaded.
 *
 */
static void openxtfbLoadCursorARGB(ScrnInfoPtr pScrn, CursorPtr pCurs)
{

    __openxtfbLoadCursorImageARGB(pScrn, pCurs->bits->argb, pCurs->bits->width,
        pCurs->bits->height, pCurs->bits->xhot, pCurs->bits->yhot);
}

#endif


/**
 * Loads the foreground and backgroudn colors requested by the X server.
 *
 * @param pScrn The screen whose cursor colors are to be modified.
 * @param bg The background color.
 * @param fg The background color.
 */
static void openxtfbSetCursorColors(ScrnInfoPtr pScrn, int bg, int fg)
{
    //Compute the desired FG and BG colors-- these are essentially the same
    //as the colors provided by X, but with the dummy byte replaced with
    //a maximum possible alpha ("solid").
    int bg_color = 0xFF000000 | bg;
    int fg_color = 0xFF000000 | fg;

    //Get a referfence to the driver's internal data structure.
    openxtfbPtr oxtPtr = (openxtfbPtr)pScrn->driverPrivate;

    //... and store the colors inside.
    oxtPtr->cursorBG = bg_color;
    oxtPtr->cursorFG = fg_color;
}


/**
 * Renders the X cursor in a format that can be interpreted by the "hardware"--
 * that is, by the OpenXT framebuffer driver.
 */
static unsigned char * openxtfbCreateARGBCursor(openxtfbPtr oxtPtr, char * source_bits)
{
    int byte_number, bit_number;
    unsigned char *target;
    uint32_t * target_pixel;

    //Compute the total amount of memory the resultant cursor image will use.
    size_t target_size = openxtfb_cursor_width_pixels
      * openxtfb_cursor_height_pixels
      * openxtfb_bytes_per_pixel;

    //... and compute the number of bytes we'll need to walk through.
    size_t source_size =
      (openxtfb_cursor_width_pixels * openxtfb_cursor_height_pixels) / 8;

    //Determine the location of the cursor mask, which should follow
    //the cursor in memory.
    char * mask_bits = source_bits + source_size;

    //... and allocate an appropriate amount of memory.
    target = calloc(1, target_size);
    if(!target)
      return NULL;

    //Establish a running pointer, which will always point to the target pixel.
    target_pixel = (uint32_t *)target;

    //For each byte in the source bitmask...
    for(byte_number = 0; byte_number < source_size; ++byte_number) {

        //Get the bytes containing the bits we'll be working with.
        unsigned char source_byte = source_bits[byte_number];
        unsigned char mask_byte   = mask_bits[byte_number];

        //Iterate through each bit in the byte.
        for(bit_number = 0; bit_number < 8; ++bit_number) {
            int source_set = (source_byte >> bit_number) & 1;
            int mask_set   = (mask_byte   >> bit_number) & 1;

            //If the mask is zero for this pixel, the pixel should be transparent.
            if(!mask_set) {
                *target_pixel = 0;
            }
            //If the mask is one, and the source is one, this is a foreground
            //pixel.
            else if(source_set) {
                *target_pixel = oxtPtr->cursorFG;
            }
            //Otherwise, it must be a background pixel.
            else {
                *target_pixel = oxtPtr->cursorBG;
            }

            //Move on to the next pixel.
            ++target_pixel;
        }
    }

    //Finally, once we've moved through the whole mask, we're done!
    //Return the resultant image.
    return target;
}



/**
 * Loads a cursor image into the PV cursor image buffer.
 * Accepts the output of openxtfbRealizeCursor.
 *
 * @param pScrn The screen whose PV cursor is to be loaded.
 * @param src The cursor data produced by openxtfbRealizeCursor.
 */
static void openxtfbLoadCursorImage(ScrnInfoPtr pScrn, unsigned char *src)
{
    //Get a referfence to the driver's internal data structure.
    openxtfbPtr oxtPtr = (openxtfbPtr)pScrn->driverPrivate;

    //Create a new ARGB curosr image from the X11 source bits...
    char * new_image =openxtfbCreateARGBCursor(oxtPtr, src);

    //... load it into the PV cursor...
    __openxtfbLoadCursorImageARGB(pScrn, new_image, 
        openxtfb_cursor_width_pixels, openxtfb_cursor_height_pixels, 
        oxtPtr->cursorHotspotX, oxtPtr->cursorHotspotY);

    //... and discard it.
    free(new_image);
}

/**
 * Show the PV cursor, rendering the cursor visible.
 *
 * @param pScrn The screen for which the PV cursor should be made visible.
 */
static void openxtfbShowCursor(ScrnInfoPtr pScrn)
{
    //Get our file descriptor, which provides our linkage to the driver.
    int fb_fd = fdForScrn(pScrn);
    ioctl(fb_fd, OPENXTFB_IOCTL_SET_CURSOR_VISIBLITY, 1);
}

/**
 * Hide the PV cursor, rendering the cursor invisible.
 *
 * @param pScrn The screen for which the PV cursor should be made invisible.
 */
static void openxtfbHideCursor(ScrnInfoPtr pScrn)
{
    //Get our file descriptor, which provides our linkage to the driver.
    int fb_fd = fdForScrn(pScrn);
    ioctl(fb_fd, OPENXTFB_IOCTL_SET_CURSOR_VISIBLITY, 0);
}


/**
 * Set the position of the PV cursor.
 *
 * @param pScrn The screen whose PV cursor is to be moved.
 * @param x The X coordiate for the cursor's new location.
 * @param y The Y coordinate for the cursor's new location.
 *
 */
static void openxtfbSetCursorPosition(ScrnInfoPtr pScrn, int x, int y)
{
    //Get our file descriptor, which provides our linkage to the driver.
    int fb_fd = fdForScrn(pScrn);

    //Create our cursor location packet...
    struct openxtfb_cursor_location location = {
        .x = x,
        .y = y
    };

    //... and send it down to the driver.
    ioctl(fb_fd, OPENXTFB_IOCTL_MOVE_CURSOR, &location);
}


/**
 * Attempt to set up PV ("hardware") cursor support for
 * the OpenXT framebuffer.
 */
static int openxtfbInitializePvCursor(ScreenPtr pScreen)
{
    int rc;
    xf86CursorInfoPtr infoPtr;

    xf86DrvMsg(pScreen->myNum, X_INFO, "Using PV cursor.\n");

    //Get a referfence to the driver's internal data structure.
    ScrnInfoPtr pScrn = xf86Screens[pScreen->myNum];
    openxtfbPtr oxtPtr = (openxtfbPtr)pScrn->driverPrivate;

    //Create a new "hardware" cursor object, which we'll register with X
    //to add PV cusrsor support.
    infoPtr = xf86CreateCursorInfoRec();
    if(!infoPtr) {
        xf86DrvMsg(pScreen->myNum, X_ERROR, "Could not allocate a PV cursor "
            "information structure! Falling back to SW cursor.\n");
        return FALSE;
    }

    //Store a copy of the PV cursor information for later use.
    oxtPtr->pvCursor = infoPtr;

    //Ensure that no cursor larger than the OpenXT-fb maximum is loaded...
    infoPtr->MaxWidth = openxtfb_cursor_width_pixels;
    infoPtr->MaxHeight = openxtfb_cursor_height_pixels;

    //... and specify that we're using a fairly standard bitmap cursor.
    infoPtr->Flags = HARDWARE_CURSOR_AND_SOURCE_WITH_MASK |
                     HARDWARE_CURSOR_SOURCE_MASK_NOT_INTERLEAVED;

    //"Register" the PV cursor functions, which will be used to render
    //a "hardware" PV cursor.
    infoPtr->UseHWCursor = openxtfbUsePVCursor;
    infoPtr->SetCursorColors = openxtfbSetCursorColors;
    infoPtr->SetCursorPosition = openxtfbSetCursorPosition;
    infoPtr->LoadCursorImage = openxtfbLoadCursorImage;
    infoPtr->HideCursor = openxtfbHideCursor;
    infoPtr->ShowCursor = openxtfbShowCursor;

#ifdef ARGB_CURSOR
    infoPtr->UseHWCursorARGB = openxtfbUsePVCursorARGB;
    infoPtr->LoadCursorARGB = openxtfbLoadCursorARGB;
#endif

    rc = xf86InitCursor(pScreen, infoPtr);
    if(!rc)
    {
        xf86DrvMsg(pScreen->myNum, X_ERROR, "Could not initialize a PV cursor "
            "! Falling back to a SW cursor.\n");
        xf86DestroyCursorInfoRec(infoPtr);
        oxtPtr->pvCursor = NULL;
    }
    return rc;

}

/**
 * Initailizes an openxtfb screen.
 *
 * @param scrnIndex A unique identifier for the given screen.
 * @param pScreen A reference to the Xorg screen object.
 * @param argc, argv Any command line parameters provided. Unused.
 */
static Bool openxtfbScreenInit(SCREEN_INIT_ARGS_DECL)
{
    VisualPtr visual;
    int init_picture = 0;
    int ret, flags;
    int type;
    int rc;

    //Get a reference to the current screen, and to this driver's
    //private information.
    ScrnInfoPtr pScrn = xf86Screens[pScreen->myNum];
    openxtfbPtr oxtPtr = (openxtfbPtr)pScrn->driverPrivate;

    //Apply the Display Handler's preferred mode as our initial mode.
    xf86DrvMsg(pScreen->myNum, X_INFO, "Setting initial mode...\n");
    xf86CrtcSetMode(oxtPtr->crtc, pScrn->currentMode, RR_Rotate_0, 0, 0);

    //First, map the openxtfb video memory, for direct use by X.
    oxtPtr->fbmem = fbdevHWMapVidmem(pScrn);
    if(!oxtPtr->fbmem)
    {
        xf86DrvMsg(pScreen->myNum, X_ERROR, "Mapping video memory failed!\n");
        return FALSE;
    }

    //And get the offset inside of the mapped memory at which the framebuffer
    //should exist, and the location where the actual framebuffer starts.
    oxtPtr->fboff = fbdevHWLinearOffset(pScrn);
    oxtPtr->fbstart = oxtPtr->fbmem + oxtPtr->fboff;

    //Save the initial state of the hardware, so we can restore to it
    //on close.
    fbdevHWSave(pScrn);

    //Initialize the hardware, switching to the mode appropriate for
    //our current display.
    rc = fbdevHWModeInit(pScrn, pScrn->currentMode);
    if(!rc)
    {
        xf86DrvMsg(pScreen->myNum, X_ERROR, "Mode initialization failed!\n");
        return FALSE;
    }

    //Enusre the screen is on (and not DPMS'd off).
    fbdevHWSaveScreen(pScreen, SCREEN_SAVER_ON);

    //And center our current frame at the framebuffer corner.
    fbdevHWAdjustFrame(ADJUST_FRAME_ARGS(pScrn, 0, 0));

    //
    //Set up the machine independent (mi) parts of our DDX.
    //

    //Set up the visuals, which will allow X to render to our framebuffer.
    miClearVisualTypes();
    if(!miSetVisualTypes(pScrn->depth, TrueColorMask, pScrn->rgbBits, TrueColor))
    {
        xf86DrvMsg(pScreen->myNum, X_ERROR, "visual type setup failed for %d bits "
            "per pixel [1]\n", pScrn->bitsPerPixel);
        return FALSE;
    }
    if(!miSetPixmapDepths())
    {
        xf86DrvMsg(pScreen->myNum, X_ERROR, "pixmap depth setup failed\n");
        return FALSE;
    }

    //Ensure that our backing store's stride matches the stride of the framebuffer.
    pScrn->displayWidth = oxtPtr->stride;

    //Finally, call the generic framebuffer screen initialization, which will
    //populate any optional methods not explicitly populated by this driver
    //with sane defaults.
    rc = fbScreenInit(pScreen, oxtPtr->fbstart, pScrn->virtualX, pScrn->virtualY,
       pScrn->xDpi, pScrn->yDpi, pScrn->displayWidth, pScrn->bitsPerPixel);

    //If we're using any DirectColor visuals,
    //ensure that they're populated with the correct color information.
    visual = pScreen->visuals + pScreen->numVisuals;
    while(--visual >= pScreen->visuals)
    {
        if((visual->class | DynamicClass) == DirectColor)
        {
            visual->offsetRed   = pScrn->offset.red;
            visual->offsetGreen = pScrn->offset.green;
            visual->offsetBlue  = pScrn->offset.blue;
            visual->redMask     = pScrn->mask.red;
            visual->greenMask   = pScrn->mask.green;
            visual->blueMask    = pScrn->mask.blue;
        }
    }

    //Bind our framebuffer to the Xorg framebuffer extension.
    rc = fbPictureInit(pScreen, NULL, 0);
    if(!rc) {
        xf86DrvMsg(pScrn->scrnIndex, X_WARNING, "Render extension initialisation failed\n");
    }

    //Initialize the screen...
    xf86SetBlackWhitePixels(pScreen);
    xf86SetBackingStore(pScreen);

    //Set up a software cursor. This will be used whenever we can't use a PV
    //cursor (e.g. if the cursor is larger than 64x64, or the host reports
    //no support for hardware cursors).
    miDCInitialize(pScreen, xf86GetPointerScreenFuncs());

    //If we have a side channel with which to send hardware cursor information,
    //set up a PV cursor.
    if(oxtPtr->fd >= 0)
      openxtfbInitializePvCursor(pScreen);

    //Finally, set up a color-map; used for DirectColor rendering.
    rc = miCreateDefColormap(pScreen);
    if(!rc)
    {
        xf86DrvMsg(pScreen->myNum, X_ERROR, "internal error: miCreateDefColormap failed "
                   "in openxtScreenInit()\n");
        return FALSE;
    }

    //set up RandR 1.2 support
    xf86CrtcScreenInit(pScreen);

    //set up our use the color-map.
    rc = xf86HandleColormaps(pScreen, 256, 8, fbdevHWLoadPaletteWeak(), NULL, CMAP_PALETTED_TRUECOLOR);
    if(!rc)
        return FALSE;

    //Use the local framebuffer's save functionality...
    pScreen->SaveScreen = fbdevHWSaveScreenWeak();

    //... and wrap the closeScreen function, so we
    //can tear down everything when the screen is closed.
    oxtPtr->CloseScreen = pScreen->CloseScreen;
    pScreen->CloseScreen = openxtfbCloseScreen;

    return TRUE;
}

/**
 * Closes a screen object handled by the OpenXT framebuffer,
 * cleaning up its resources.
 *
 * @param scrnIndex A unique identifier describing the screen to be torn down.
 * @param pScreen An information structure describing the screen to be torn down.
 *
 */
static Bool openxtfbCloseScreen(CLOSE_SCREEN_ARGS_DECL)
{
    //Get a reference to the current screen, and to this driver's
    //private information.
    ScrnInfoPtr pScrn = xf86ScreenToScrn(pScreen);
    openxtfbPtr oxtPtr = (openxtfbPtr)pScrn->driverPrivate;

    //If we were able to set up a PV cursor, tear that down...
    if(oxtPtr->pvCursor) {

        //Ensure that the Display Handler no longer renders the cursor,
        //as it still exists in the DH's memory on the other side until
        //another application initializes a hardware cursor...
        openxtfbHideCursor(pScrn);

        //... and clean up the cursor's memory.
        xf86DestroyCursorInfoRec(oxtPtr->pvCursor);
        oxtPtr->pvCursor = NULL;
    }

    //If we had a side-channel to the openxt framebuffer, close it.
    if(oxtPtr->fd) {
        close(oxtPtr->fd);
        oxtPtr->fd = -1;
    }

    //Tear down our connection to the openxt framebuffer...
    fbdevHWRestore(pScrn);
    fbdevHWUnmapVidmem(pScrn);

    //... restor the original tear-down fucntion...
    pScrn->vtSema = FALSE;
    pScreen->CloseScreen = oxtPtr->CloseScreen;

    //... and execute the core tear-down routines.
    return (*pScreen->CloseScreen)(CLOSE_SCREEN_ARGS);
}


/**
 * Generic driver properties query functions.
 */
static Bool openxtfbDriverFunction(ScrnInfoPtr pScrn, xorgDriverFuncOp op, pointer ptr)
{
    xorgHWFlags *flag;

    switch(op)
    {
        //"Returns" a list of required hardware interfaces via the output pointer.
        case GET_REQUIRED_HW_INTERFACES:
            
            //We don't require any particular hardware, as we're virtualized.
            flag = (CARD32 *)ptr;
            (*flag) = 0;
            return TRUE;

        //Fail if we don't recognize the query.
        default:
            return FALSE;
    }
}


/**
 * Handles the process of switching modes. This function is typically
 * used to set up hardware to display the new mode; in our case, we
 * delegate the actual mode switch to our RandR resize handler.
 */
static Bool openxtfbSwitchMode(SWITCH_MODE_ARGS_DECL)
{
    SCRN_INFO_PTR(arg);

    //Trigger our RandR mode-setting handler.
    return xf86SetSingleMode(pScrn, mode, RR_Rotate_0);
}
