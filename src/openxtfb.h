/**
 * OpenXT Framebuffer ("openxtfb") Driver for X11
 * Authors: Kyle J. Temkin <temkink@ainfosec.com>
 *
 * Based on the fbdev driver, by:
 * Authors:
 *       Alan Hourihane, <alanh@fairlite.demon.co.uk>
 *       Michel Dänzer, <michel@tungstengraphics.com>
 */

#ifndef __OPENXTFB_H__
#define __OPENXTFB_H__

#define OPENXTFB_VERSION       1000
#define OPENXTFB_NAME          "OpenXT-FB"
#define OPENXTFB_DRIVER_NAME   "openxtfb"

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <string.h>
#include <linux/fb.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

/* all driver need this */
#include "xf86.h"
#include "xf86_OSproc.h"

#include "mipointer.h"
#include "micmap.h"
#include "colormapst.h"
#include "xf86cmap.h"
#include "shadow.h"
#include "dgaproc.h"

/* for visuals */
#include "fb.h"

#if GET_ABI_MAJOR(ABI_VIDEODRV_VERSION) < 6
#include "xf86Resources.h"
#include "xf86RAC.h"
#endif

#include "fbdevhw.h"

#include "xf86xv.h"
#include "xf86Cursor.h"
#include "cursorstr.h"

//Allow direct hardware access to OpenXT framebuffer devices.
#include <sys/ioctl.h>
#include <linux/openxtfb.h>

//Include the API compatibility functions, which allow our driver
//to run on mobile Xorg APIs.
#include "compat-api.h"

//Forward declare our main data structures.
struct openxtfbObject;
typedef struct openxtfbObject openxtfbRec;
typedef struct openxtfbObject *openxtfbPtr;

//Allow us to support RandR.
#include "randr.h"

/**
 * Define the options that can be provided in xorg.conf sections:
 * - fbdev: The framebuffer device to be used for rendering.
 */
static const OptionInfoRec openxtfbOptions[] =
{
    { 0  , "fbdev" , OPTV_STRING , {0} , FALSE },
    { -1 , NULL    , OPTV_NONE   , {0} , FALSE }
};

/**
 * Quick constant: the number of bytes per pixel on an openxtfb
 * backend framebuffer.
 */
static const int openxtfb_bytes_per_pixel = sizeof(uint32_t);

/**
 * Forward declarations.
 */
static const OptionInfoRec *openxtfbGetPossibleOptions(int chipid, int busid);
static void openxtfbIdentify(int flags);
static Bool openxtfbProbe(DriverPtr drv, int flags);
static Bool openxtfbPreInit(ScrnInfoPtr pScrn, int flags);
static Bool openxtfbScreenInit(SCREEN_INIT_ARGS_DECL);
static Bool openxtfbCloseScreen(CLOSE_SCREEN_ARGS_DECL);
static Bool openxtfbDriverFunction(ScrnInfoPtr pScrn, xorgDriverFuncOp op, pointer ptr);
static Bool openxtfbSwitchMode(SWITCH_MODE_ARGS_DECL);

/**
 * Private information structure for openxt framebuffers.
 */
struct openxtfbObject
{
    //References to the underlying OpenXT framebuffer's memory.
    unsigned char *fbstart;
    unsigned char *fbmem;

    //The offset into the OpenXT framebuffer at which rendering
    //should begin.
    int fboff;

    //Store the fixed size information regarding the OpenXT framebuffer.
    //These are guaranteed by the driver to remain consistent for the life
    //of the framebuffer.
    int virtualX;
    int virtualY;
    int stride;

    //The file descriptor used to communicate with the framebuffer.
    //Used for exchanging hardware cursor IOCTLs.
    int fd;

    //A reference to the method used to clean up screens handled by
    //the driver. We store a reference to this method internally, so we
    //can wrap it with our own custom function.
    CloseScreenProcPtr CloseScreen;

    //Store a structure describing the PV cursor in use, if any.
    xf86CursorInfoPtr pvCursor;

    //Store a structure describing the active virtual CRTC
    //and output.
    xf86CrtcPtr crtc;

    //Store the cursor background and foreground color. Used for two-color
    //cursors.
    int cursorFG;
    int cursorBG;

    //Store the cursor's current hotspot.
    int cursorHotspotX;
    int cursorHotspotY;

    //And store information about the underlying X options.
    EntityInfoPtr pEnt;
    OptionInfoPtr Options;

};

#endif
